//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Oforkanji Odekpe on 1/8/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
    }

}
